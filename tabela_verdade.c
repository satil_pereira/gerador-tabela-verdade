// Esse programa criara uma tabela verdade com base na entrada dadas
// Pensarei em eventuais solucoes extras para o programa executar

#include <stdio.h>
#include <math.h>

long int deci_to_binary(int num1, int num2, int num3);

int conjuncao(int x, int y);

int disjuncao(int a, int b);

int disjExclusiva(int p, int q);

int implicacao(int antec, int conseq);

// Struct apra trabalhar com os valores verdade de cada linha
typedef struct
{
    char simbolo;
    int valor;
}
valorVerdade;

int main(void)
{
    // Varios textos para explicar as paradas né
    printf("Funcionamento:\n  Para utilizar o programa, voce tera que digitar o numero de\n  caracteres que ira precisar, isso inclue os proposicionais atomicos\n  e os conectivos.\n\n");
    printf("  (p > q) equivale a: p entao q. nesse caso, temos 5 caracteres: '(', 'p', '>', 'q', ')'.\n\n");

    // Uma tabela basica com os simbolos que o prog suporta
    printf("\nTabela de simbolos disponiveis:\ne = \t\t\t\t\t\t\t| '&'\nou(inclusivo = \t\t\t\t\t\t| '|'\nou(exclusivo) = \t\t\t\t\t| '+'\nproposicionais atomicos(minusculo), de 'a' a 'z': \t| 'a', 'b', 'c', ..., 'x', 'y', 'z'.\nentao = \t\t\t\t\t\t| '>'\nse e somente se = \t\t\t\t\t| ':'");
    printf("\nabre parenteses = \t\t\t\t\t| '('\nfecha parenteses = \t\t\t\t\t| ')'\nvirgula = \t\t\t\t\t\t| ','\nnegacao = \t\t\t\t\t\t| '!'\n\n");
    printf("OBS.: nao utilizar os '', Ex.: se quiser usar o entao, use apenas >\n\n");
    // VAriavel pro cara que le a quantidade de simbolos necessarios
    int qtdChar;
    printf("Digite o numero de simbolos necessarios: ");
    scanf("%d", &qtdChar);
    char simbolo[qtdChar];
    // Caracter criado para conferir se foi tudo digitado corretamente
    char check;

    do
    {
        for (int i = 0; i < qtdChar; i++)
        {
            setbuf(stdin, NULL);
            printf("Digite o %do simbolo: ", i + 1);
            scanf("%c", &simbolo[i]);
        }

        // Printa a formula digitada e muda alguns simbolos para facilitar a leitura
        printf("\nSua formula eh: \"");
        for (int i = 0; i < qtdChar; i++)
        {
            switch (simbolo[i])
            {
                case '>':
                printf(" -> ");
                break;

                case ':':
                printf(" <-> ");
                break;

                case '&':
                printf(" && ");
                break;

                case '|':
                printf(" || ");
                break;

                case '!':
                printf("%c", 170);
                break;

                default:
                printf("%c", simbolo[i]);
                break;
            }
        }
        // Só uma linha
        printf("\"\n\n");

        do
        {
            // Confirma se a formula esta correta antes de prosseguir
            printf("Sua formula esta correta?(S/N): ");
            setbuf(stdin, NULL);
            scanf("%c", &check);
        }
        while (check != 's' && check != 'S' && check != 'n' && check != 'N');
        // SE houver erro na digitacao, solicitara a formula de novo. E importante para o programa que a formula seja
        // Bem formada, caso contrario, ele nao ira funcionar direito
        if (check == 'n' || check == 'N')
        {
            printf("\nDigite os simbolos novamente... \n");
        }
    }
    while (check != 's' && check != 'S');
    // Mais uma linha
    printf("\n");

    int qtdAtomo;

    // Identifica simbolos proposicionais atomicos
    for (int i = 0; i < qtdChar; i++)
    {
        if (simbolo[i] >= 'a' && simbolo[i] <= 'z')
        {
            qtdAtomo++;
        }
    }
    // Cria uma variavel para cada um dos atomos, de forma a declarar seus valores verdade
    char propAtomico[qtdAtomo];
    // Count ira adicionar para a variavel proposicional atomica
    int count = 0;
    for (int i = 0; i < qtdChar; i++)
    {
        if (simbolo[i] >= 'a' && simbolo[i] <= 'z')
        {
            propAtomico[count] = simbolo[i];
            count++;
        }
    }
    // Imprime o cabecalho da tabela
    for (int i = 0; i < qtdAtomo; i++)
    {
        printf("%c ", propAtomico[i]);
    }

    for (int i = 0; i < qtdChar; i++)
    {
        if (simbolo[i] == '!')
            printf("%c%c", 170, simbolo[i + 1]);
    }

    printf(" ");

    for (int i = 0; i < qtdChar; i++)
    {
        switch (simbolo[i])
            {
                case '>':
                printf(" -> ");
                break;

                case ':':
                printf(" <-> ");
                break;

                case '&':
                printf(" && ");
                break;

                case '|':
                printf(" || ");
                break;

                case '!':
                printf("%c", 170);
                break;

                default:
                printf("%c", simbolo[i]);
                break;
            }
    }
    // Outra linha
    printf("\n");
    // Utilizando o tipo de dado criado. limitado a no maximo 5 proposicionais atomicos
    valorVerdade tabela[5];

    int potAtomo = pow(2, qtdAtomo);
    // Printa a tabela
    for (int i = 0; i < potAtomo; i++)
    {
        // Imprime os atomos
        for (int j = qtdAtomo; j > 0; j--)
        {
            if (deci_to_binary(i, qtdAtomo, j - 1) == 0)
            {
                tabela[j - 1].simbolo = propAtomico[j - 1];
                tabela[j - 1].valor = 0;
                printf("V ");
            }

            else
            {
                tabela[j - 1].simbolo = propAtomico[j - 1];
                tabela[j - 1].valor = 1;
                printf("F ");
            }
        }

        if (simbolo[i] == '(')
        {
            for (int j = i; i < qtdChar; j++)
            {
                while (simbolo[i] != ')')
                {
                    switch (simbolo[j])
                    {
                        case '!':
                            for (int k = 0; k < qtdAtomo; k++)
                            {
                                if (simbolo[j + 1] == tabela[k].simbolo)
                                {
                                    if (tabela[k].valor == 0)
                                    printf("F ");

                                    else
                                    printf("V ");
                                }
                            }
                        break;

                        // ta saindo fora do espaço da array mno tabela[k - 2].simbolo, refazer

                        /*case '>':
                            for (int k = 0; k < qtdAtomo; k++)
                            {
                                if (simbolo[j + 1] == tabela[k].simbolo && simbolo[j + 1] == tabela[k - 2].simbolo)
                                {
                                    if (implicacao(tabela[k].valor, tabela[k - 2].valor) == 0)
                                        printf("V ");

                                    else
                                        printf("F ");
                                }
                            }
                        break;*/

                    }
                }
            }

        }
        // Prossegue para a proxima linha
        printf("\n");
    }
}













// A variavel num1 indica o numero a ser transformado em binario
long int deci_to_binary(int num1, int num2, int num3)
{
    float resto;
    long int binary[num2];
    // Algoritmo de conversão para binário
    for (int i = 0; i < num2; i++)
    {
        resto = (float)num1 / 2;
        resto -= (int)resto;
        resto *= 2;

        if (resto == 1)
            binary[i] = 1;

        else
            binary[i] = 0;

        num1 = (num1 - resto) / 2;
    }

    return binary[num3];
}

// Nesses casos, estou utilizando 0(ZERO) como verdadeiro
int conjuncao(int x, int y)
{
    if (x == 0 && y == 0)
        return 0;

    else
        return 1;
}

int disjuncao(int a, int b)
{
    if (a == 1 || b == 1)
        return 0;

    else
        return 1;
}

int disjExclusiva(int p, int q)
{
    if (p == 1 && q == 0)
        return 0;

    else if (p == 0 && q == 1)
        return 0;

    else
        return 1;
}

int implicacao(int antec, int conseq)
{
    if (antec == 0)
        return 0;

    else if(antec == 0 && conseq == 0)
        return 0;

    else
        return 1;
}





